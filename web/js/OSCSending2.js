
var checkboxVal = false;
var checkbox = [];

document.addEventListener('DOMContentLoaded', function () {
    checkbox = document.querySelector('input[type="checkbox"]');
    checkbox.checked = false;

    checkbox.addEventListener('change', function () {
        if (checkbox.checked) {
            checkboxVal = true;
            console.log('Checked');
        } else {
            checkboxVal = false;
            console.log('Not checked');
        }
    });
});

var port = new osc.WebSocketPort({
    url: "ws://192.168.1.10:8081"
});


port.open();

function Play(number) {
    port.send({
        address: "/cue/{"+number.toString()+"}/go"
    });
}

function Stop(number) {
    for (let ii=0; ii<number; ii++){
        port.send({
            address: "/cue/{"+ii.toString()+"}/panic"
        });
    }
    
}

function Play1() {
    port.send({
        address: "/cue/{1}/go"
    });
}

function Play2() {
    port.send({
        address: "/cue/{2}/go"
    });
}

function Play3() {
    port.send({
        address: "/cue/{3}/go"
    });
}

function Fade1() {
    port.send({
        address: "/cue/{1.1}/go"
    });
}

function Stop1() {
    port.send({
        address: "/cue/{1.2}/go"
    });
}

function Stop2() {
    port.send({
        address: "/cue/{2.2}/go"
    });
}

function Stop3() {
    port.send({
        address: "/cue/{3.2}/go"
    });
}

document.addEventListener('DOMContentLoaded', function (event) {
$('#volume').on('change input', function() {
    let val = $(this).val();
    port.send({
        address: "/cue/{1}/sliderLevel/0",
        args: [
            {
                type: 'f',
                value: val
            }
        ]
    });
});
});

document.addEventListener('DOMContentLoaded', function (event) {
    $('#volume2').on('change input', function() {
        let val = $(this).val();
        port.send({
            address: "/cue/{2}/sliderLevel/0",
            args: [
                {
                    type: 'f',
                    value: val
                }
            ]
        });
    });
    });

    document.addEventListener('DOMContentLoaded', function (event) {
        $('#volume3').on('change input', function() {
            let val = $(this).val();
            port.send({
                address: "/cue/{3}/sliderLevel/0",
                args: [
                    {
                        type: 'f',
                        value: val
                    }
                ]
            });
        });
        });


        document.addEventListener('DOMContentLoaded', function (event) {
            $('#volumeB').on('change input', function() {
                let dBVal = document.getElementById('dbVal');
                let val = $(this).val();
                let valdisp = Math.round(10*(100*val-90))/10;
                let txt = [" " + valdisp.toString() + " dB"];
                dbVal.textContent = txt;
                port.send({
                    address: "/ch/15/mix/fader",
                    args: [
                        {
                            type: 'f',
                            value: val
                        }
                    ]
                });
            });
            });