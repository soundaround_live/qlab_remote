# QLab Remote
Web Remote for QLab.

### Software Requirements 
The following software has to be installed:
- QLab [1]
- node.js [2]

### Manual
- Make sure that your MacBook running QLab and the remote device are in the same network.
- Note that the IDs of the Play-Cues in QLab have to be integer numbers starting with 1. In this demonstration, 12 play buttons are prepared but you can add as many buttons you like by adding a play button to *index.html* with the onclick action onclick="Play(#ID)" replacing #ID with the play button ID of QLab.
- Open the file *OSCSending2.js* from the js folder (inside the web folder) and modify the IP address to the IP address of the QLaB-Macbook accordingly (line 21). 
- Open the file *index.js* from the root folder with node.js (for **MacOS** users: navigate to the folder in your terminal and use *node .*). 
- Enter the IP address of the QLab-MacBook in any browser of your remote device. 

### Licence
This sotware is released under [MIT](LICENSE) licence.<br>
Copyright &copy; Lukas Gölles, 2022

### References
[1] https://qlab.app/ <br>
[2] https://nodejs.org/en/